package models;


public class GebruikerInstance {

    public String Gebruikersnaam;
    public String Wachtwoord;
    public String Organisatie;
    public String CIN;
    public String Achternaam;
    public String Geslacht;
    public String Geboortedatum;
    public String Trajectnaam;
    public String Trajectcode;



    // nodig om json in te laden
    @SuppressWarnings("unused")
    private GebruikerInstance(){}

    public GebruikerInstance(String gebruikersnaam, String wachtwoord, String organisatie, String CIN, String achternaam, String geslacht, String geboortedatum, String trajectnaam, String trajectcode) {
        Gebruikersnaam = gebruikersnaam;
        Wachtwoord = wachtwoord;
        Organisatie = organisatie;
        this.CIN = CIN;
        Achternaam = achternaam;
        Geslacht = geslacht;
        Geboortedatum = geboortedatum;
        Trajectnaam = trajectnaam;
        Trajectcode = trajectcode;
    }

    public void setTrajectcode(String trajectcode) {
        this.Trajectcode = trajectcode;
    }

    public String getTrajectcode() {
        return Trajectcode;
    }

    public void setTrajectnaam(String trajectnaam) {
        this.Trajectnaam = trajectnaam;
    }

    public String getTrajectnaam() {
        return Trajectnaam;
    }


    public String getGebruikersnaam() {
        return Gebruikersnaam;
    }

    public void setGebruikersnaam(String gebruikersnaam) {
        Gebruikersnaam = gebruikersnaam;
    }

    public String getWachtwoord() {
        return Wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        Wachtwoord = wachtwoord;
    }

    public String getOrganisatie() {
        return Organisatie;
    }

    public void setOrganisatie(String organisatie) {
        Organisatie = organisatie;
    }

    public String getCIN() {
        return CIN;
    }

    public void setCIN(String CIN) {
        this.CIN = CIN;
    }

    public String getAchternaam() {
        return Achternaam;
    }

    public void setAchternaam(String achternaam) {
        Achternaam = achternaam;
    }

    public String getGeslacht() {
        return Geslacht;
    }

    public void setGeslacht(String geslacht) {
        Geslacht = geslacht;
    }

    public String getGeboortedatum() {
        return Geboortedatum;
    }

    public void setGeboortedatum(String geboortedatum) {
        Geboortedatum = geboortedatum;
    }

}
