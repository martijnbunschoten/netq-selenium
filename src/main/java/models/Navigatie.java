package models;

import utilities.Testbase;

public class Navigatie extends Testbase {

    private static String LoginPagina = "https://netqromtest.netqhealthcare.nl";
    private static String ClientSearchEvent = "https://netqromtest.netqhealthcare.nl/index.cfm?event=clientSearch&cin=";
    private static String VerwijderClientSearchEvent = "https://netqromtest.netqhealthcare.nl/index.cfm?event=deleteClient&clientID=";
    private static String VerwijderInstrumentEvent = "https://netqromtest.netqhealthcare.nl/index.cfm?event=deleteInstrument&instrumentID=";
    private static String TrajectVerwijderenEvent = "https://netqromtest.netqhealthcare.nl/index.cfm?event=deleteMonitorProgram&monitorProgramID=";


    public static String GetLoginPage() {
        return LoginPagina;
    }

    public static String GetClientSearchEvent() {
        return ClientSearchEvent + gebruiker.getCIN();
    }


    public static String GetVerwijderClientSearchEvent() {
        return VerwijderClientSearchEvent;
    }

    public static String getVerwijderInstrumentEvent() {
        return VerwijderInstrumentEvent;
    }

    public static String getTrajectVerwijderenEvent() {
        return TrajectVerwijderenEvent;
    }

}


