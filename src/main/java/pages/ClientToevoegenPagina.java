package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class ClientToevoegenPagina extends BasisPagina {


    // Client toevoegen
    private By OpslaanKnop = By.xpath("//button[@class=\"btn btn-success\"]");

    // basisgegevens
    private By CINVeld = By.id("CIN");
    private By AchternaamVeld = By.id("lastname");
    private By GeslachtVeld = By.id("gender");
    private By MannelijkGeslacht = By.xpath("//option[contains(text(), 'Man')]");
    private By VrouwelijkGeslacht = By.xpath("//option[contains(text(), 'Vrouw')]");
    private By GeboortedatumVeld = By.id("dateofbirth");


    public ClientToevoegenPagina VulCINin(String CIN) {
        VulTekstIn(CINVeld, CIN);
        return this;
    }

    public ClientToevoegenPagina VulAchternaamIn(String achternaam) {
        VulTekstIn(AchternaamVeld, achternaam);

        ExtentLogger.log(Status.INFO, "Ik vul de achternaam van de client in");

        return this;
    }

    public ClientToevoegenPagina SelecteerGeslacht(String geslacht) {

        KlikOpElement(GeslachtVeld);

        switch (geslacht) {
            case "Man":
                KlikOpElement(MannelijkGeslacht);
                break;
            case "Vrouw":
                KlikOpElement(MannelijkGeslacht);
                break;
            default:
                KlikOpElement(MannelijkGeslacht);
                break;
        }
//        if ("Man".equals(geslacht)) {
//            KlikOpElement(MannelijkGeslacht);
//        } else if (geslacht == "Vrouw") {
//            KlikOpElement(VrouwelijkGeslacht);
//        }

        ExtentLogger.log(Status.INFO, "Ik selecteer het geslacht");

        return this;
    }

    public ClientToevoegenPagina VulGeboortedatumIn(String geboortedatum) {
        VulTekstIn(GeboortedatumVeld, geboortedatum);

        ExtentLogger.log(Status.INFO, "Ik vul de geboortedatum van de client in");

        return this;
    }

    public DashboardPagina KlikOpOpslaan() {
        KlikOpElement(OpslaanKnop);

        ExtentLogger.log(Status.INFO, "Vervolgens klik ik op opslaan en ververs ik de clientenpagina");

        return new DashboardPagina();
    }

}
