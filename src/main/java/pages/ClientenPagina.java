package pages;

import com.aventstack.extentreports.Status;
import models.Navigatie;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class ClientenPagina extends BasisPagina {

    private By ClientToevoegenKnop = By.xpath("//button[contains(text(),'toevoegen')]");

    // Client informatie
    private static By Achternaam = By.xpath("//td[contains(text(), 'clienttoevoegen')]");

    // Client verwijderen
    public static By ClientID = By.xpath("//button[@class='btn btn-success one-time-measurement']");

    private static By ZoekFunctieVeldClient = By.id("combinedsearch");
    private static By GeenZoekResultaatVoorClient = By.xpath("//div[@class='alert alert-warning']");
    private static By TrajectClient = By.xpath("//td[contains(text(), 'Trajecttestingjoop')]");


    public ClientToevoegenPagina KlikOpClientToevoegen() {
        KlikOpElement(ClientToevoegenKnop);

        ExtentLogger.log(Status.INFO, "Vanuit de clientenpagina navigeer ik naar de clienttoevoegenpagina");

        return new ClientToevoegenPagina();
    }


    public static void VerwijderClient() {
        GaNaarUrl(Navigatie.GetClientSearchEvent());
        String OphalenClientID = HaalTekstVanAttribuutOp(ClientID, "data-client-id");
        GaNaarUrl(Navigatie.GetVerwijderClientSearchEvent() + OphalenClientID);
    }


    public static void VerifeerOfClientVerwijderdIs() {
        VulTekstIn(ZoekFunctieVeldClient, gebruiker.getAchternaam());
        DrukOpEnter(ZoekFunctieVeldClient);

        if(HaalTekstOp(GeenZoekResultaatVoorClient).equals("Uw zoekopdracht heeft geen resultaten opgeleverd.")) {
            System.out.println("Client succesvol verwijderd");
        } else {
            ExtentLogger.log(Status.WARNING, "Client is niet verwijderd, schoon testdata op.");
            System.out.println("Client is niet verwijderd, schoon testdata op");
        }
    }


    public static String HaalAchternaamOp() {
        String VerifieerAchternaam = HaalTekstOp(Achternaam);
        return VerifieerAchternaam;
    }

    public ClientOverzichtPagina KlikOpClientVoorBehandelTraject() {
        DubbelKlikOpElement(TrajectClient);
        return new ClientOverzichtPagina();
    }

    public ClientOverzichtPagina KlikOpClientVoorEenmaligeMeting() {
        DubbelKlikOpElement(TrajectClient);

        ExtentLogger.log(Status.INFO, "Ik selecteer een client");
        return new ClientOverzichtPagina();
    }


}
