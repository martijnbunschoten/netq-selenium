package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class DashboardPagina extends BasisPagina{


    private static By ClientenIcoon = By.id("clientIcoon");
    private static By PaginaHeader = By.xpath("//h1[@class='page-header']");
    private static By InstrumentenIcoon = By.id("instrumentIcoon");
    private static By UitlogKnop = By.xpath("//i[@class='icon icon-sign-out']");
    private static By OkuitlogKnop = By.xpath("//button[@class='btn btn-lg btn-success']");
    private static By TrajectbeheerIcoon = By.id("trajectIcoon");


    public void UitloggenAlsGebruiker() {
        KlikOpElement(UitlogKnop);
        KlikOpElement(OkuitlogKnop);
    }

    public static String  HaalPaginaHeaderOp() {
        String Header = HaalTekstOp(PaginaHeader);
        return Header;
    }

    public TrajectbeheerPagina KlikOpTrajectbeheerIcoon() {
        KlikOpElement(TrajectbeheerIcoon);

        ExtentLogger.log(Status.INFO, "Vanuit de dashboardpagina navigeer ik naar de trajectbeheer pagina");

        return new TrajectbeheerPagina();
    }

    public ClientenPagina KlikOpClientenIcoon() {
        KlikOpElement(ClientenIcoon);

        ExtentLogger.log(Status.INFO, "Vanuit de dashboardpagina navigeer ik naar de clientenpagina");

        return new ClientenPagina();
    }

    public InstrumentenPagina KlikOpInstrumentenIcoon() {
        KlikOpElement(InstrumentenIcoon);

        ExtentLogger.log(Status.INFO, "Vanuit de dashboardpagina navigeer ik naar de instrumentenpagina");

        return new InstrumentenPagina();
    }

    public ClientenPagina VerversClientenPagina() {
        KlikOpElement(ClientenIcoon);
        return new ClientenPagina();
    }

    public InstrumentenPagina VerversInstrumentenPagina() {
        KlikOpElement(InstrumentenIcoon);
        return new InstrumentenPagina();
    }

    public TrajectbeheerPagina VerversTrajectBeheerPagina() {
        KlikOpElement(TrajectbeheerIcoon);
        return new TrajectbeheerPagina();
    }


}
