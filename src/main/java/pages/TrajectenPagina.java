package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import utilities.ExtentLogger;

public class TrajectenPagina extends BasisPagina{

    // Trajectenknop bovenaan
    private static By TrajectenKnop = By.xpath("//a[contains(text(), 'Trajecten')]");

    // Kleine tandwiel voor acties op de meting
    private static By KleineTandwielKnop = By.xpath("//i[@class='icon icon-actions']");
    private static By VerwijderenKnop = By.xpath("//i[@class='icon icon-delete']");
    private static By VerwijderenOK = By.xpath("//button[contains(text(), 'OK')]");

    // Voor het asserten van de vragenlijst
    private static By KklVragenlijst =  By.xpath("//td[contains(text(), 'Eenmalige')]");



    public TrajectenPagina verversTrajectenoverzichtPagina() {
        KlikOpElement(TrajectenKnop);
        return this;
    }


    public static void VerwijderVragenlijstEenmaligeMeting() {
        KlikOpElement(KleineTandwielKnop);
        KlikOpElement(VerwijderenKnop);
        KlikOpElement(VerwijderenOK);
    }

    public static void VerifieerOfVragenLijstVerwijderdIs() {

        try {
            HaalVragenLijstOp();
            System.out.println("Vragenlijst is niet verwijderd");
            ExtentLogger.log(Status.WARNING, "Vragenlijst is niet verwijderd, schoon testdata op");
        }
        catch (NoSuchElementException e) {
            System.out.println("Vragenlijst is verwijderd");
        }

    }

    public static String HaalVragenLijstOp() {
        String Vragenlijst = HaalTekstOp(KklVragenlijst);
        return Vragenlijst;
    }


}
