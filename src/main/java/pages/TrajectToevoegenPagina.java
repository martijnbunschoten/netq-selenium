package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class TrajectToevoegenPagina extends BasisPagina {

   private static By TrajectNaamVeld = By.id("name");
   private static By TrajectcodeVeld = By.id("monitorProgramCode");
   private static By OpslaanKnop = By.xpath("//button[contains(text(), 'opslaan')]");
   private static By TrajectNaam = By.xpath("//td[contains(text(), 'Happy')]");
   private static By MetingenKnop = By.xpath("//a[contains(text(), 'Metingen')]");


   public TrajectToevoegenPagina VulTrajectNaamIn(String Trajectnaam) {
       VulTekstIn(TrajectNaamVeld, Trajectnaam);

       ExtentLogger.log(Status.INFO, "Ik vul de naam van het traject in");


       return this;
   }

   public TrajectToevoegenPagina VulTrajectCodeIn(String Trajectcode) {
       VulTekstIn(TrajectcodeVeld, Trajectcode);

       ExtentLogger.log(Status.INFO, "Ik vul de code van het traject in");

       return this;
   }

   public DashboardPagina KlikOpOpslaan() {
       KlikOpElement(OpslaanKnop);

       ExtentLogger.log(Status.INFO, "Vervolgens klik ik op opslaan en ververs ik de trajectpagina");


       return new DashboardPagina();
   }

   public String HaalTrajectNaamOp() {
     String naam = HaalTekstOp(TrajectNaam);
     return naam;
   }

   public MetingToevoegenPagina KlikOpMetingen() {
       KlikOpElement(MetingenKnop);

       ExtentLogger.log(Status.INFO, "Ik klik op metingen");

       return new MetingToevoegenPagina();
   }

}
