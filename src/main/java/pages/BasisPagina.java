package pages;

import models.Navigatie;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Driver;
import utilities.Testbase;

public class BasisPagina extends Testbase {

    // ingang vauit de test voor leesbaarheid
    public static String GaNaarPagina(String pagina) {

    switch (pagina) {
        case "LoginPagina":
           return Navigatie.GetLoginPage();

        default:
            return (pagina + " is niet bekend. Geef een geldige URL op");

      }
    }

    // Gebruik in methodes
    public static void GaNaarUrl(String Url) {
        Driver.Instance.navigate().to(Url);
    }

    public static String HaalUrlOp() {
        return Driver.Instance.getCurrentUrl();
    }

    public static void KlikOpElement(By element) {
        Driver.Instance.findElement(element).click();
    }

    public static void VulTekstIn(By element, String tekst) {
        Driver.Instance.findElement(element).sendKeys(tekst);
    }

    public static String HaalTekstOp(By element) {
        String TekstOphalen = Driver.Instance.findElement(element).getText();
        return TekstOphalen;
    }

    public static String HaalTekstVanAttribuutOp(By element, String Attribuut) {
        String TekstAttribuut = Driver.Instance.findElement(element).getAttribute(Attribuut);
        return TekstAttribuut;
    }


    public static void DubbelKlikOpElement(By element) {

        Actions actions = new Actions(Driver.Instance);
        WebElement elementLocator = Driver.Instance.findElement(element);
        actions.doubleClick(elementLocator).perform();
    }

    public static void ScrollNaarElement(By element) {

        WebElement myElement = Driver.Instance.findElement(element);
        ((JavascriptExecutor) Driver.Instance).executeScript("arguments[0].scrollIntoView();", myElement);
        Actions actions = new Actions(Driver.Instance);
        actions.moveToElement(myElement).perform();
    }

    public static void DrukOpEnter(By element) {
        Driver.Instance.findElement(element).sendKeys(Keys.ENTER);
    }

    public static void WachtTotElementGeladenIs(By element) {
        new WebDriverWait(Driver.Instance, 10).until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public static void WisselNaarEenAnderFrame(By element) {
        Driver.Instance.switchTo().frame(Driver.Instance.findElement(element));
    }


}



