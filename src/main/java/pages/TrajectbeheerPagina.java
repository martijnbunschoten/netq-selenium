package pages;

import com.aventstack.extentreports.Status;
import models.Navigatie;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class TrajectbeheerPagina extends BasisPagina{

   private static By TrajectToevoegenKnop = By.xpath("//button[contains(text(), 'Traject toevoegen')]");
   private static By ZoekFunctieVeldTraject = By.id("combinedsearch");
   private static By GeenZoekResultaatVoorTraject = By.xpath("//div[@class='alert alert-warning']");
   private static By TrajectID = By.xpath("//i[@class='icon icon-actions']");

   // apart test traject aangemaakt
   private static By StartMetingTraject = By.xpath("//td[contains(text(), 'Startmeting')]");

   public TrajectToevoegenPagina KlikOpTrajectToevoegen() {
       KlikOpElement(TrajectToevoegenKnop);

       ExtentLogger.log(Status.INFO, "Op de trajectbeheer pagina klik ik op traject toevoegen");

       return new TrajectToevoegenPagina();
   }


   public TrajectToevoegenPagina SelecteerStartMetingTraject() {
       ScrollNaarElement(StartMetingTraject);

       DubbelKlikOpElement(StartMetingTraject);

       ExtentLogger.log(Status.INFO, "Ik selecteer het traject waar ik mijn startmeting aan wil toevoegen");

       return new TrajectToevoegenPagina();
   }


   public static void VerwijderTraject() {
       VulTekstIn(ZoekFunctieVeldTraject, gebruiker.Trajectnaam);
       DrukOpEnter(ZoekFunctieVeldTraject);

       String OphalenTrajectID = HaalTekstVanAttribuutOp(TrajectID, "data-monitor-program-id");
       GaNaarUrl(Navigatie.getTrajectVerwijderenEvent() + OphalenTrajectID);
   }



   public static void VerifieerOfTrajectVerwijderdIs() {
       VulTekstIn(ZoekFunctieVeldTraject, gebruiker.Trajectnaam);
       DrukOpEnter(ZoekFunctieVeldTraject);

       if(HaalTekstOp(GeenZoekResultaatVoorTraject).equals("Uw zoekopdracht heeft geen resultaten opgeleverd.")) {
           System.out.println("Traject succesvol verwijderd");
       } else {
           ExtentLogger.log(Status.WARNING, "Traject is niet verwijderd, schoon testdata op");
           System.out.println("Traject is niet verwijderd, schoon testdata op");
       }

   }


}
