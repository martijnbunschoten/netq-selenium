package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class LoginPagina extends BasisPagina{

    private By GebruikersnaamVeld = By.id("username");
    private By WachtwoordVeld = By.id("password");
    private By OrganisatieVeld = By.id("organisatie");
    private By InloggenKnop = By.id("loginButton");


    public LoginPagina VulGebruikersnaamIn(String gebruikersnaam) {
        VulTekstIn(GebruikersnaamVeld, gebruikersnaam);
        return new LoginPagina();
    }

    public LoginPagina VulWachtwoordIn(String wachtwoord) {
        VulTekstIn(WachtwoordVeld, wachtwoord);
        return this;
    }

    public LoginPagina VulOrganisatieIn(String organisatie) {
        VulTekstIn(OrganisatieVeld, organisatie);
        return this;
    }


    public DashboardPagina KlikOpInloggen() {
        KlikOpElement(InloggenKnop);
        return new DashboardPagina();
    }

    public DashboardPagina LoginMetStandaardGebruiker(String gebruikersnaam, String wachtwoord, String organisatie) {
        VulTekstIn(GebruikersnaamVeld, gebruikersnaam);
        VulTekstIn(WachtwoordVeld, wachtwoord);
        VulTekstIn(OrganisatieVeld, organisatie);
        KlikOpElement(InloggenKnop);

        ExtentLogger.log(Status.INFO, "Ik vul mijn gegevens in en klik op inloggen");

        return new DashboardPagina();
    }

}
