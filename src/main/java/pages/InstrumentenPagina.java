package pages;

import com.aventstack.extentreports.Status;
import models.Navigatie;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import utilities.ExtentLogger;

public class InstrumentenPagina extends BasisPagina {

    private static By InstrumentToevoegenKnop = By.xpath("//button[contains(text(), 'Instrument toevoegen')]");
    private static By AfkortingInstrument = By.xpath("//td[contains(text(), '1234')]");
    private static By ZoekFunctieVeldInstrument = By.id("combinedsearch");
    private static By InstrumentID = By.xpath("//button[@class='btn btn-default icon icon-actions action']");
    private static By GeenZoekresultaatInstrument = By.xpath("(//div[@class='alert alert-warning'])[1]");
    private static By InstrumentHeaderVoorVerificatie = By.xpath("//h1[contains(text(), 'test1234')]");

    public  InstrumentToevoegenPagina KlikOpInstrumentToevoegenKnop() {
       KlikOpElement(InstrumentToevoegenKnop);

        ExtentLogger.log(Status.INFO, "Op de instrumentenpagina klik ik op instrument toevoegen");

    return new InstrumentToevoegenPagina();
}


   public static String HaalAfkortingInstrumentOp () {
        String Afkorting = HaalTekstOp(AfkortingInstrument);
        return Afkorting;
   }


   public static void VerwijderInstrument() {
        VulTekstIn(ZoekFunctieVeldInstrument, "Test1234");
        DrukOpEnter(ZoekFunctieVeldInstrument);
        String OphalenInstrumentID = HaalTekstVanAttribuutOp(InstrumentID, "data-instrument-id");
        GaNaarUrl(Navigatie.getVerwijderInstrumentEvent() + OphalenInstrumentID);
   }

   public static void VerifeerOfInstrumentVerwijderdIs() {

        DrukOpEnter(ZoekFunctieVeldInstrument);

        try {
            HaalTekstOp(InstrumentHeaderVoorVerificatie);
            System.out.println("Instrument is niet verwijderd, schoon testdata op");
            ExtentLogger.log(Status.WARNING, "Instrument is niet verwijderd, schoon testdata op");
        }
        catch (NoSuchElementException ex) {
            System.out.println("Instrument succesvol verwijderd");
        }
   }

   }
