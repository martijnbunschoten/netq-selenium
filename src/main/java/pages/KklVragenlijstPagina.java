package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utilities.Driver;
import utilities.ExtentLogger;

import java.util.List;
import java.util.Set;

public class KklVragenlijstPagina extends BasisPagina {


    public static final String TEST = "Test";
    private static By Verder = By.xpath("//input[@value='Verder']");
    private static By Verstuur = By.xpath("//input[@value='Verstuur']");
    private static By Radioknoppen = By.cssSelector(".nqradio");
    private static By WelkeVerslavingVeld = By.xpath("//input[@name='q2_1']");
    private static By AndereKlachtenVeld = By.xpath("//input[@name='q5_1']");


    public TrajectenPagina VulKklVragenLijstIn() {

        //wisselen van window
        String VragenlijstWinHandle = Driver.Instance.getWindowHandle();


        List<WebElement> elements = Driver.Instance.findElements(Radioknoppen);
             for(WebElement webElement :  elements) {

           webElement.click();
        }
        KlikOpElement(Verder);


        List<WebElement> elements2 = Driver.Instance.findElements(Radioknoppen);
        for(WebElement webElement2 : elements2 ) {
            webElement2.click();
        }
        KlikOpElement(Verder);

        List<WebElement> elements3 = Driver.Instance.findElements(Radioknoppen);
        for(WebElement webElement3 : elements3 ) {
            webElement3.click();
        }
        KlikOpElement(Verder);

        List<WebElement> elements4 = Driver.Instance.findElements(Radioknoppen);
        for(WebElement webElement4 : elements4 ) {
            webElement4.click();
        }
        VulTekstIn(WelkeVerslavingVeld, TEST);
        VulTekstIn(AndereKlachtenVeld, TEST);
        KlikOpElement(Verstuur);

        Set<String> winHandles = Driver.Instance.getWindowHandles();

        for(String handle: winHandles) {
            if(!handle.equals(VragenlijstWinHandle)) {
                Driver.Instance.switchTo().window(handle);
                System.out.println("Title of new window: " + Driver.Instance.getTitle());
            }
        }
        ExtentLogger.log(Status.INFO, "Ik vul de gekozen vragenlijst in");
        return new TrajectenPagina();
    }

}
