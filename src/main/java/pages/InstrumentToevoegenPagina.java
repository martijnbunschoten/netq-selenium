package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class InstrumentToevoegenPagina extends BasisPagina {

   // NetQVragenlijst
   private static By NetQVragenLijstVeld = By.id("Qnr_ID");
   private static By HealthMonitorVragenlijst = By.xpath("//option[contains(text(), 'Health Monitor')]");

   // Afkorting
   private static By AfkortingVeld = By.id("abbr");

   // Categorie
   private static By CategorieVeld = By.id("instrumentCategoryID");

   // Type
   private static By TypeVeld = By.id("instrumentTypeID");

   // Opslaan
   private static By OpslaanKnop = By.xpath("//button[@class='btn btn-success']");


   public InstrumentToevoegenPagina SelecteerNetQVragenlijst() {
      KlikOpElement(NetQVragenLijstVeld);
      KlikOpElement(HealthMonitorVragenlijst);

      ExtentLogger.log(Status.INFO, "Ik selecteer een instrument");

      return this;
   }

   public InstrumentToevoegenPagina VulAfkortingIn(String Afkorting) {
      VulTekstIn(AfkortingVeld, "Test1234");

      ExtentLogger.log(Status.INFO, "Ik vul een afkorting voor het instrument in");

      return this;
   }

   public DashboardPagina KlikOpOpslaan() {
      KlikOpElement(OpslaanKnop);

      ExtentLogger.log(Status.INFO, "Vervolgens klik ik opslaan en ververs ik de instrumentenpagina");

      return new DashboardPagina();
   }



}
