package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import utilities.ExtentLogger;

public class MetingToevoegenPagina extends BasisPagina {

    private static By MetingToevoegenKnop = By.id("add-measurement");

    // type meting
    private static By Startmeting = By.xpath("//a[@data-action='startmeting']");
    private static By Tussenmeting = By.xpath("//a[@data-action='tussenmeting']");
    private static By Eindmeting = By.xpath("//a[@data-action='eindmeting']");

    private static By ToelichtingMeting = By.xpath("//input[@name='measurementDescription']");

    private static By InstrumenToevoegenKnop = By.xpath("(//button[@class='btn btn-success add-instrument'])[1]");
    private static By SelecteerInstrumentKnop = By.xpath("//button[contains(text(), 'Select')]");
    private static By ABCL = By.xpath("(//td[contains(text(), 'ABCL')])[2]");
    private static By ADPIV = By.xpath("//td[contains(text(), 'ADP-IV Vragenlijst')]");

    private static By SelecterenKnop = By.xpath("//button[contains(text(), 'Selecteren')]");
    private static By ToevoegenKnop = By.xpath("//button[contains(text(), 'Toevoegen')]");

    private static By WijzgingenOpslaanKnop = By.id("save");

    private static By MetingenKnop = By.xpath("//a[contains(text(), 'Metingen')]");

    // toelichting string om te valideren nadat meting is aangemaakt
    private static By ToelichtingStartMeting = By.xpath("//small[contains(text(), 'Happy')]");

    // verwijderen meting
    private static By Tandwiel = By.xpath("//button[@class='btn btn-default icon icon-actions action']");
    private static By VerwijderenKnop = By.xpath("(//i[@class='icon icon-delete'])[3]");


    public MetingToevoegenPagina VoegMetingToe(String meting) {

        KlikOpElement(MetingToevoegenKnop);

        switch(meting) {

            case "startmeting":
                KlikOpElement(Startmeting);
                break;
            case "tussenmeting":
                KlikOpElement(Tussenmeting);
                break;
            case "eindmeting":
                KlikOpElement(Eindmeting);
                break;
            default:
                System.out.println("De ingevulde waarde: "+ meting + " is geen geldige waarden. Kies uit ..");
                break;
        }

        ExtentLogger.log(Status.INFO, "Ik voeg een startmeting toe");
        return this;
    }


    public MetingToevoegenPagina SelecteerInstrument(String instrument) {

        ScrollNaarElement(InstrumenToevoegenKnop);
        WachtTotElementGeladenIs(InstrumenToevoegenKnop);
        KlikOpElement(InstrumenToevoegenKnop);
        KlikOpElement(SelecteerInstrumentKnop);

        switch (instrument) {
            case "ABCL":
                KlikOpElement(ABCL);
                break;
            case "ADPIV":
                KlikOpElement(ADPIV);
                break;
        }

        KlikOpElement(SelecterenKnop);

        ExtentLogger.log(Status.INFO, "Ik selecteer het bijhorende instrument");
        return this;
    }


    public MetingToevoegenPagina VulToelichtingMetinIn(String toelichting) {
        VulTekstIn(ToelichtingMeting, toelichting);

        ExtentLogger.log(Status.INFO, "Ik vul de toelichting op de meting in");

        return this;
    }

    public MetingToevoegenPagina KlikOpToevoegen() {
        WachtTotElementGeladenIs(ToevoegenKnop);
        KlikOpElement(ToevoegenKnop);
        return this;
    }

    public MetingToevoegenPagina KlikOpWijzgingenOpslaan() {
        KlikOpElement(WijzgingenOpslaanKnop);

        ExtentLogger.log(Status.INFO, "Vervolgens klik ik op opslaan en ververs ik de metingpagina");

        return this;
    }


    public void VerversMetingenPagina() {
        KlikOpElement(MetingenKnop);
    }

    public static String HaalToelichtingStartMetingOp() {
        String toelichting = HaalTekstOp(ToelichtingStartMeting);
        return toelichting;
    }

    public static void VerwijderMeting() {
        KlikOpElement(Tandwiel);
        KlikOpElement(VerwijderenKnop);
        KlikOpElement(WijzgingenOpslaanKnop);
    }


    public static void VerifieerOfMetingVerwijderdIs() {
        KlikOpElement(MetingenKnop);

        try {
            HaalToelichtingStartMetingOp();
            System.out.println("Meting is niet verwijderd, schoon testdata op");
            ExtentLogger.log(Status.WARNING, "Meting is niet verwijderd, schoon testdata op");

        } catch (NoSuchElementException ex) {
            System.out.println("Meting is verwijderd ");
        }
    }



}
