package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import utilities.Driver;
import utilities.ExtentLogger;
import java.util.Set;

public class ClientOverzichtPagina extends BasisPagina {

    private static By LagerOnderwijs = By.cssSelector(".nqoption");

    // bovenaan menu links "clientoverzicht"
    private static By ClientOverzichtKnop = By.xpath("//a[contains(text(), 'Cliëntoverzicht')]");

    // De groene knoppen rechts op de pagina
    private static By BehandelTrajectKnop = By.xpath("//button[@class='btn btn-success add-treatment']");
    private static By EenmaligeMetingKnop = By.xpath("//button[@class='btn btn-success one-time-measurement']");

    // Naam van het traject en de grote tandwiel knop om het traject te verwijderen
    private static By TrajectNaam = By.xpath("//h2[@class='caption']");
    private static By GroteTandwielKnop = By.xpath("//button[@class='btn btn-default icon icon-actions action']");
    private static By BehandelTrajectVerwijderen = By.xpath("//i[@class='icon icon-delete']");
    private static By VerwijderenOK = By.xpath("//button[contains(text(), 'OK')]");

    // Naam van de eenmalige meting en de status
    private static By EenmaligeMetingH2Naam = By.xpath("//h2[@class='caption']");
    private static By StatusEenMaligeMeting = By.xpath("//tr[@class='clickableRow']//td[5]");

    // Kleine tandwiel voor het afnemen van de meting
    private static By KleineTandwielKnop = By.xpath("//i[@class='icon icon-actions']");
    private static By AfnemenMetingKnop = By.xpath("//a[@data-action='conductMeasurement']");
    private static By VerwijderenKnop = By.xpath("//i[@class='icon icon-delete']");

    // Vragenlijst Iframe
    private static By RomIFrame = By.cssSelector("#romTemplateIFrame");


    public BehandelTrajectToevoegenPagina KlikOpBehandelTrajectToevoegen() {
        KlikOpElement(BehandelTrajectKnop);

        ExtentLogger.log(Status.INFO, "Ik klik op behandeltraject toevoegen");

        return new BehandelTrajectToevoegenPagina();
    }


    public static String HaalTrajectNaamOp() {
        //String Traject = HaalTekstOp(TrajectNaam);
        String Traject = HaalTekstVanAttribuutOp(TrajectNaam, "class");
        return Traject;
    }

    public ClientOverzichtPagina VerversClientOverzichtPagina() {
        KlikOpElement(ClientOverzichtKnop);
        return this;
    }


    public static void VerwijderBehandelTraject() {
        KlikOpElement(GroteTandwielKnop);
        KlikOpElement(BehandelTrajectVerwijderen);
        KlikOpElement(VerwijderenOK);
    }

    public static void VerifieerOfBehandelTrajectVerwijderdIs() {

        try {
            HaalTekstOp(TrajectNaam);
            System.out.println("Traject is niet verwijderd, schoon testdata op");
            ExtentLogger.log(Status.WARNING, "Traject is niet verwijderd, schoon testdata op");
        }
        catch (NoSuchElementException ex) {
            System.out.println("Behandeltraject is succesvol verwijderd");
        }
    }


    public EenmaligeMetingToevoegenPagina KlikOpEenmaligeMetingToevoegen() {
        KlikOpElement(EenmaligeMetingKnop);
        ExtentLogger.log(Status.INFO, "Ik klik op eenmalige meting toevoegen");

        return new EenmaligeMetingToevoegenPagina();
    }

    public static String HaalH2elementOpVanEenmaligeMeting() {
       String meting = HaalTekstOp(EenmaligeMetingH2Naam);
       return meting;
    }

    public static String HaalStatusVanEenmaligeMetingOp() {
        String status = HaalTekstOp(StatusEenMaligeMeting);
        return status;
    }

    public KklVragenlijstPagina KlikOpAfnemenMeting() {

        String parentWinHandle = Driver.Instance.getWindowHandle();
        System.out.println("Parent window handle: " + parentWinHandle);

        KlikOpElement(KleineTandwielKnop);
        KlikOpElement(AfnemenMetingKnop);

        Set<String> winHandles = Driver.Instance.getWindowHandles();

        for(String handle: winHandles) {
            if(!handle.equals(parentWinHandle)) {
                Driver.Instance.switchTo().window(handle);
                System.out.println("Title of new window: " + Driver.Instance.getTitle());
            }
        }

        WachtTotElementGeladenIs(RomIFrame);
        WisselNaarEenAnderFrame(RomIFrame);

        ExtentLogger.log(Status.INFO, "Vervolgens klik ik op afnemen meting");
        return new KklVragenlijstPagina();
    }

}
