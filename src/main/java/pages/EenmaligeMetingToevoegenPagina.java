package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class EenmaligeMetingToevoegenPagina extends BasisPagina {

    private static By MetingtypeVeld = By.name("measurementtypeID");
    private static By Startmeting = By.xpath("//option[contains(text(), 'Startmeting')]");
    private static By Tussenmeting = By.xpath("//option[contains(text(), 'Tussenmeting')]");
    private static By Eindmeting = By.xpath("//option[contains(text(), 'Eindmeting')]");
    private static By UitnodigingVeld = By.name("notificationDefID");
    private static By StandaardSysteemUitnodiging = By.xpath("(//option[@class='defaultOptionNETQROM'])[1]");
    private static By MaakDirectMetingAanKnop = By.xpath("//button[contains(text(), 'Maak direct een')]");
    private static By AfkortingVeldBijKiezenInstrument = By.name("abbr");
    private static By KorteKlachtenLijst = By.xpath("(//td[contains(text(), 'Korte Klachten Lijst')])[1]");
    private static By ZoekenKnop = By.xpath("//button[contains(text(), 'Zoeken')]");
    private static By SelecteerInstrumentenKnop = By.xpath("//button[@class='btn btn-lg btn-success']");


   public EenmaligeMetingToevoegenPagina SelecteerInstrument() {
       VulTekstIn(AfkortingVeldBijKiezenInstrument, "KKL");
       KlikOpElement(ZoekenKnop);
       WachtTotElementGeladenIs(KorteKlachtenLijst);
       KlikOpElement(KorteKlachtenLijst);
       ScrollNaarElement(SelecteerInstrumentenKnop);
       KlikOpElement(SelecteerInstrumentenKnop);

       ExtentLogger.log(Status.INFO, "Ik selecteer een instrument");

       return this;
   }


   public EenmaligeMetingToevoegenPagina SelecteerMetingtype(String metingtype) {
       KlikOpElement(MetingtypeVeld);

       switch(metingtype) {
           case "Startmeting":
               KlikOpElement(Startmeting);
               break;
           case "Tussenmeting":
               KlikOpElement(Tussenmeting);
               break;
           case "Eindmeting":
               KlikOpElement(Eindmeting);
               break;
           default: KlikOpElement(Startmeting);
       }

       ExtentLogger.log(Status.INFO, "Ik selecteer het meting type");
       return this;
   }


   public EenmaligeMetingToevoegenPagina SelecteerUitnodigingStandaardSysteem() {

       KlikOpElement(UitnodigingVeld);
       KlikOpElement(StandaardSysteemUitnodiging);

       ExtentLogger.log(Status.INFO, "Ik selecteer de uitnodiging voor de client");

       return this;
   }

   public ClientOverzichtPagina KlikOpMaakDirectEenMetingAan() {
       KlikOpElement(MaakDirectMetingAanKnop);

       ExtentLogger.log(Status.INFO, "Ik klik op maak direct een meting aan");

       return new ClientOverzichtPagina();
   }



}
