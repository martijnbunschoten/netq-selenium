package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import utilities.ExtentLogger;

public class BehandelTrajectToevoegenPagina extends BasisPagina {

    private static By TrajectVeld = By.id("monitorProgramTrajectID");
    private static By StartMetingHappy = By.xpath("//option[contains(text(), 'Startmetinghappy')]");
    private static By OpslaanEnActiveren = By.id("activateButton");


    public BehandelTrajectToevoegenPagina SelecteerTraject() {
        KlikOpElement(TrajectVeld);
        KlikOpElement(StartMetingHappy);

        ExtentLogger.log(Status.INFO, "Ik selecteer het traject");

        return this;
    }

   public ClientOverzichtPagina KlikOpOpslaanEnActiveren() {
        KlikOpElement(OpslaanEnActiveren);

        ExtentLogger.log(Status.INFO, "Vervolgens klik op slaan en activeer ik het behandeltraject");

        return new ClientOverzichtPagina();
   }
}




