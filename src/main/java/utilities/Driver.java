package utilities;

import config.BrowserConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class Driver {

    public static WebDriver Instance = null;

    public static void Initialize() {

        if(Instance == null) {
            System.out.println("Initializing driver");
            if(BrowserConfiguration.browserChoice.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                Instance = new ChromeDriver();
            }
            else if (BrowserConfiguration.browserChoice.equalsIgnoreCase("ie")) {
                System.setProperty("webdriver.ie.driver", "src/main/resources/IEDriverServer.exe");
                Instance = new InternetExplorerDriver();
            }

            else if (BrowserConfiguration.browserChoice.equalsIgnoreCase("firefox")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                Instance = new FirefoxDriver();
            } else {
                System.out.println("No driver found...");
            }
        }

        Instance.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        Instance.manage().window().maximize();

    } // end method

    public static void Close() {
        System.out.println("Closing browser");
        Instance.close();
        Instance = null;
    }

    public static void Quit() {
        System.out.println("Quitting the browser");
        Instance.quit();
        Instance = null;
    }

}
