package utilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.GebruikerInstance;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.BasisPagina;
import pages.LoginPagina;
import java.io.File;
import java.io.IOException;

public class Testbase {

    public static ExtentHtmlReporter reporter;
    public static ExtentReports extent;

    protected static GebruikerInstance gebruiker;

    protected static GebruikerInstance getNewGebruiker(String fileName) {
        final ObjectMapper mapper = new ObjectMapper();
        final File file = new File(fileName);
        try {
            gebruiker = mapper.readValue(file, GebruikerInstance.class);
        } catch (IOException e) {
            gebruiker = new GebruikerInstance("1","2","3","4","5","6","7", "8", "9");
        }
        // (oude manier) new Gebruiker(gebruiker);
        return gebruiker;
    }


    public static void setupReport(){
        System.out.println("Initializing report...");

        // Create Object of ExtentHtmlReporter and provide the path where you want to generate the report
        reporter = new ExtentHtmlReporter("./Reports/Reports.html");

        // Attach the reporter which we created in Step 1
        reporter.setAppendExisting(true);

        // Config reporter
        reporter.config().setDocumentTitle("NetQ Selenium Testen");
        //reporter.setAppendExisting(true);
        reporter.config().setTheme(Theme.STANDARD);
        // reporter.loadXMLConfig(new File("src/main/resources/extent-config.xml"));

        // Create object of ExtentReports class- This is main class which will create report
        extent = new com.aventstack.extentreports.ExtentReports();

        // attach the reporter which we created in Step 1
        extent.attachReporter(reporter);
    }

    public static void shutdownReport(){
        System.out.println("Flush report...");
        extent.flush(); // Flush method will write the test in report- This is mandatory step
        // System.out.println("Report can be found at: " + );
    }

    @BeforeClass
    public static void Setup() {
        // initializing driver
        Driver.Initialize();

        // initializing gebruiker object
        gebruiker = getNewGebruiker("src/main/resources/file.json");

        // initializing report
        setupReport();

    }

    @AfterClass
    public static void Cleanup() {
        // closes current browser session
        Driver.Close();

        // flushes report
        shutdownReport();

        // Closes current instance of the driver
        // Driver.Quit();
    }

    public static LoginPagina NavigeerNaar(String pagina) {

        Driver.Instance.navigate().to(BasisPagina.GaNaarPagina(pagina));
        return new LoginPagina();
    }

    }


