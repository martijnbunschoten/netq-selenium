package utilities;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


import static utilities.Testbase.extent;

public class ExtentLogger {

    private static ExtentTest extentTest;

    public static void create (String testCase) {
        extentTest = extent.createTest(testCase);
    }

    public static void log (Status status, String text) {
        extentTest.log(status, text);
    }

}
