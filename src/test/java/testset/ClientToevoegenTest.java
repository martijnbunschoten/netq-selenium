package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.ClientenPagina;
import utilities.ExtentLogger;
import utilities.Testbase;

public class ClientToevoegenTest extends Testbase {

    @Test
    public void ClientToevoegen() {

                // Maak test aan met logger
                ExtentLogger.create("ClientToevoegenTest");


                NavigeerNaar("LoginPagina")

                .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

                .KlikOpClientenIcoon()
                .KlikOpClientToevoegen()
                .VulCINin(gebruiker.getCIN())
                .VulAchternaamIn(gebruiker.getAchternaam())
                .SelecteerGeslacht(gebruiker.getGeslacht())
                .VulGeboortedatumIn(gebruiker.getGeboortedatum())
                .KlikOpOpslaan()
                .VerversClientenPagina();

                 // Asserts en loggers
                 if (ClientenPagina.HaalAchternaamOp().equals(gebruiker.getAchternaam())) {
                     ExtentLogger.log(Status.PASS, "Client met achternaam " + gebruiker.getAchternaam() + " is succesvol aangemaakt");
                 } else {
                     ExtentLogger.log(Status.FAIL, "Achternaam is niet gevonden en de test is gefaald");
                 }
    }

    @AfterClass
    public static void VerwijderClient(){
        ClientenPagina.VerwijderClient();
        ClientenPagina.VerifeerOfClientVerwijderdIs();
    }
}
