package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.TrajectenPagina;
import utilities.ExtentLogger;
import utilities.Testbase;


public class VragenlijstAfnemenTest extends Testbase {

   @Test
   public void VragenLijstAfnemen() {

       ExtentLogger.create("VragenlijstAfnemenTest");

       NavigeerNaar("LoginPagina")

       .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

       .KlikOpClientenIcoon()
       .KlikOpClientVoorEenmaligeMeting()
       .KlikOpEenmaligeMetingToevoegen()
       .SelecteerInstrument()
       .SelecteerMetingtype("Startmeting")
       .SelecteerUitnodigingStandaardSysteem()
       .KlikOpMaakDirectEenMetingAan()
       .KlikOpAfnemenMeting()
       .VulKklVragenLijstIn()
       .verversTrajectenoverzichtPagina();

     if(TrajectenPagina.HaalVragenLijstOp().equals("Eenmalige Meting KKL")) {
         ExtentLogger.log(Status.PASS, "De vragenlijst is succesvol ingevuld");
     } else {
         ExtentLogger.log(Status.FAIL, "De vragenlijst is niet goed ingevuld");
     }
   }

    @AfterClass
    public static void EenmaligeMetingVerwijderen() {
        TrajectenPagina.VerwijderVragenlijstEenmaligeMeting();
        TrajectenPagina.VerifieerOfVragenLijstVerwijderdIs();
    }
}
