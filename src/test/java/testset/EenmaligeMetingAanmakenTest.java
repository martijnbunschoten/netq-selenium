package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.ClientOverzichtPagina;
import utilities.ExtentLogger;
import utilities.Testbase;

public class EenmaligeMetingAanmakenTest  extends Testbase {

    @Test
    public void EenmaligeMetingAanmaken() {

        ExtentLogger.create("EenmaligeMetingAanmakenTest");

        NavigeerNaar("LoginPagina")

        .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())
        .KlikOpClientenIcoon()
        .KlikOpClientVoorEenmaligeMeting()
        .KlikOpEenmaligeMetingToevoegen()
        .SelecteerInstrument()
        .SelecteerMetingtype("Startmeting")
        .SelecteerUitnodigingStandaardSysteem()
        .KlikOpMaakDirectEenMetingAan();


        if(ClientOverzichtPagina.HaalH2elementOpVanEenmaligeMeting().equals("Eenmalige Meting AB") || (ClientOverzichtPagina.HaalStatusVanEenmaligeMetingOp().equals("Verzonden"))) {
            ExtentLogger.log(Status.PASS, "Het aanmaken van een eenmalige meting is succesvol");
        } else {
            ExtentLogger.log(Status.FAIL, "De eenmalige meting is niet aangemaakt");
        }

    }


    @AfterClass
    public static void EenmaligeMetingVerwijderen() {

        ClientOverzichtPagina.VerwijderBehandelTraject();
        ClientOverzichtPagina.VerifieerOfBehandelTrajectVerwijderdIs();
    }
}
