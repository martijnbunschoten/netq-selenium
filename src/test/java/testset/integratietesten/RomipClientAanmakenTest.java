package testset.integratietesten;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

public class RomipClientAanmakenTest {

    @Test
    public void RomipClientAanmaken() {

        given().
                when().
                get("http://ergast.com/api/f1/2017/circuits.json").
                then().
                assertThat().
                body("MRData.CircuitTable.Circuits.circuitId",hasSize(20)).
                and().
                statusCode(200);
    }
      }

