package testset;

import com.aventstack.extentreports.Status;
import org.junit.FixMethodOrder;
import org.junit.Test;
import pages.DashboardPagina;
import utilities.ExtentLogger;
import utilities.Testbase;


public class LoginTest extends Testbase {


    @Test
    public void InloggenMetGebruiker() {

        ExtentLogger.create("LoginTest");

          NavigeerNaar("LoginPagina")

          .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie());


         if(DashboardPagina.HaalPaginaHeaderOp().equals("Dashboard")) {
              ExtentLogger.log(Status.PASS, "Logintest is geslaagd");
         } else {
              ExtentLogger.log(Status.FAIL, "Logintest is gefaald");
         }

    }

}
