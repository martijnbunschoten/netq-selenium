package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.MetingToevoegenPagina;
import utilities.ExtentLogger;
import utilities.Testbase;

public class StartMetingToevoegenTest extends Testbase {


    @Test
    public void StartMetingToevoegen() {

        ExtentLogger.create("StartMetingToevoegenTest");

        NavigeerNaar("LoginPagina")

        .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

        .KlikOpTrajectbeheerIcoon()
        .SelecteerStartMetingTraject()
        .KlikOpMetingen()
        .VoegMetingToe("startmeting")
        .VulToelichtingMetinIn("Happytrajecttesting")
        .SelecteerInstrument("ABCL")
        .KlikOpToevoegen()
        .KlikOpWijzgingenOpslaan()
        .VerversMetingenPagina();


        if(MetingToevoegenPagina.HaalToelichtingStartMetingOp().equals("- Happytrajecttesting")) {
            ExtentLogger.log(Status.PASS, "Het aanmaken van een startmeting is geslaagd");
        } else {
            ExtentLogger.log(Status.FAIL, "Het aanmaken van een startmeting is niet geslaagd");
            System.out.println(MetingToevoegenPagina.HaalToelichtingStartMetingOp());
        }
    }

    @AfterClass
    public static void VerwijderMeting() {
        MetingToevoegenPagina.VerwijderMeting();
        MetingToevoegenPagina.VerifieerOfMetingVerwijderdIs();
    }

}
