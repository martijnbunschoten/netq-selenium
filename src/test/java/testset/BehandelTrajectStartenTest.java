package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.ClientOverzichtPagina;
import utilities.ExtentLogger;
import utilities.Testbase;

public class BehandelTrajectStartenTest extends Testbase {


     @Test
     public void BehandelTrajectStarten() {

           ExtentLogger.create("BehandelTrajectStartenTest");

           NavigeerNaar("LoginPagina")

           .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

           .KlikOpClientenIcoon()
           .KlikOpClientVoorBehandelTraject()
           .KlikOpBehandelTrajectToevoegen()
           .SelecteerTraject()
           .KlikOpOpslaanEnActiveren();

           if(ClientOverzichtPagina.HaalTrajectNaamOp().equals("caption")) {
               ExtentLogger.log(Status.PASS, "Behandeltraject is succesvol gestart");
           } else {
               ExtentLogger.log(Status.FAIL, "Behandeltraject starten is niet gelukt");
           }
       }

      @AfterClass
      public static void BehandelTrajectVerwijderen() {
           ClientOverzichtPagina.VerwijderBehandelTraject();
           ClientOverzichtPagina.VerifieerOfBehandelTrajectVerwijderdIs();
       }
}
