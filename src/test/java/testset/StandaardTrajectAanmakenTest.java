package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.TrajectbeheerPagina;
import utilities.ExtentLogger;
import utilities.Testbase;

public class StandaardTrajectAanmakenTest extends Testbase {

       @Test
       public void StandaardTrajectAanmaken() {

           ExtentLogger.create("StandaardTrajectAanmakenTest");

            NavigeerNaar("LoginPagina")
           .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

           .KlikOpTrajectbeheerIcoon()
           .KlikOpTrajectToevoegen()
           .VulTrajectNaamIn(gebruiker.getTrajectnaam())
           .VulTrajectCodeIn(gebruiker.getTrajectcode())
           .KlikOpOpslaan()
           .VerversTrajectBeheerPagina();


           if (gebruiker.getTrajectnaam().equals("Happy traject")) {
               ExtentLogger.log(Status.PASS, "Het aanmaken van een standaard traject is geslaagd");
           } else {
               ExtentLogger.log(Status.FAIL, "Het aannmaken van een standaard traject is niet geslaagd");
           }
       }

       @AfterClass
       public static void VerwijderTraject() {
           TrajectbeheerPagina.VerwijderTraject();
           TrajectbeheerPagina.VerifieerOfTrajectVerwijderdIs();
       }
}



