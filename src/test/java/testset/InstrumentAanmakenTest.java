package testset;

import com.aventstack.extentreports.Status;
import org.junit.AfterClass;
import org.junit.Test;
import pages.InstrumentenPagina;
import utilities.ExtentLogger;
import utilities.Testbase;


public class InstrumentAanmakenTest extends Testbase {

    @Test
    public void InstrumentAanmaken() {

         ExtentLogger.create("InstrumentAanmakenTest");

          NavigeerNaar("LoginPagina")
         .LoginMetStandaardGebruiker(gebruiker.getGebruikersnaam(), gebruiker.getWachtwoord(), gebruiker.getOrganisatie())

         .KlikOpInstrumentenIcoon()
         .KlikOpInstrumentToevoegenKnop()
         .SelecteerNetQVragenlijst()
         .VulAfkortingIn("Test1234")
         .KlikOpOpslaan()
         .VerversInstrumentenPagina();


        if(InstrumentenPagina.HaalAfkortingInstrumentOp().equals("Test1234")) {
            ExtentLogger.log(Status.PASS, "Instrument aanmaken test is geslaagd");
        } else {
            ExtentLogger.log(Status.FAIL, "Het aanmaken van een instrument is niet geslaagd");
        }
    }

    @AfterClass
    public static void VerwijderInstrument() {
        InstrumentenPagina.VerwijderInstrument();
        InstrumentenPagina.VerifeerOfInstrumentVerwijderdIs();
    }
}
